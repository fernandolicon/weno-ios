//
//  WOFRestaurantDetailViewController.m
//  WenoApp
//
//  Created by Luis Fernando Mata on 23/6/15.
//  Copyright (c) 2015 Weno. All rights reserved.
//

#import <MapKit/MapKit.h>
#import "WOFRestaurantDetailViewController.h"
//#import "WOFRestaurantMenuViewController.h"
#import "WOFRestaurantListCollectionViewController.h"
#import "WOFRestaurantTableViewCell.h"
#import "WOFObjectCoreDataHelper.h"
#import "WOFFoodCollectionViewController.h"
#import "WOFViewHelper.h"
#import "WOFRestaurant.h"
#import <JGProgressHUD/JGProgressHUD.h>
#import "Haneke.h"

@interface WOFRestaurantDetailViewController () <WOFObjectCoreDataHelperDelegate, UITableViewDataSource, UITableViewDelegate, UIScrollViewDelegate>{
    WOFObjectCoreDataHelper *objectDataHelper;
    CGFloat lastOffset;
    JGProgressHUD *progressHUD;
    NSString *contactInfo;
    NSString *phoneNumber;
    NSString *moreInfo;
    NSString *wiFiPassword;
    BOOL isRegisteredUser;
}

@end

@implementation WOFRestaurantDetailViewController

@synthesize detailRestaurant;
@synthesize isFavorite;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self visualSetup];
    
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
    
    //No registered users can't save favorites
    NSString *logInMethod = [[NSUserDefaults standardUserDefaults] objectForKey:@"logInMethod"];
    isRegisteredUser = [logInMethod isEqualToString:@"NoMethod"] ? NO : YES;
    
    objectDataHelper = [[WOFObjectCoreDataHelper alloc] init];
    objectDataHelper.delegate = self;
    
    progressHUD = [JGProgressHUD progressHUDWithStyle:JGProgressHUDStyleDark];
    progressHUD.textLabel.text = @"Guardando";
    
    isFavorite = [objectDataHelper isRestaurantwithIDFavorite:detailRestaurant.objectID];
    
    lastOffset = self.infoTableView.contentOffset.y;
    
    [self setRestaurantDetail];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) viewWillAppear:(BOOL)animated{
    [self.navigationController setNavigationBarHidden:YES animated:YES];
    
    self.navigationItem.title = detailRestaurant.name;
    self.restaurantText.text = detailRestaurant.name;
}

#pragma mark - Set data

- (void) setRestaurantDetail{
    contactInfo = [NSString stringWithFormat:@"%@\n%@", detailRestaurant.address, detailRestaurant.typeMenu];
    phoneNumber = detailRestaurant.phone;
    moreInfo = [NSString stringWithFormat:@"Vestimenta: %@\nZona: %@", detailRestaurant.codeDress, detailRestaurant.zone];
    
    wiFiPassword = detailRestaurant.wifiPassword;
    
    if (!isFavorite) {
        [self.bannerImage hnk_setImageFromURL:detailRestaurant.bannerPhoto];
        [self.logoImage hnk_setImageFromURL:detailRestaurant.primaryPhoto];
    }else{
        NSDictionary *restaurantImages = [objectDataHelper restaurantPicturesWithID:detailRestaurant.objectID];
        self.bannerImage.image = [UIImage imageWithData:[restaurantImages objectForKey:@"banner"]];
        self.logoImage.image = [UIImage imageWithData:[restaurantImages objectForKey:@"logo"]];
    }
}

#pragma mark Visual setup
-(void)visualSetup{
    self.infoTableView.contentInset = UIEdgeInsetsMake(self.bannerImage.bounds.size.height, 0, 0, 0);
    self.infoTableView.tableFooterView = [[UIView alloc] init];
}

-(void)viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];
    self.infoTableView.contentInset = UIEdgeInsetsMake(self.bannerImage.bounds.size.height, 0, 100, 0);
}

#pragma mark - Scroll view delegate
-(void)scrollViewDidScroll:(UIScrollView *)scrollView{
    
    if (scrollView.contentOffset.y > lastOffset) {
        self.blurView.alpha += .015;
    }else{
        self.blurView.alpha -= .015;
    }
    
    if (scrollView.contentOffset.y < -200) {
        self.blurView.alpha = 0.05;
    }
    
    if (scrollView.contentOffset.y < 10) {
        [self.navigationController setNavigationBarHidden:YES animated:YES];
        self.backButton.hidden = NO;
        
    }else if (scrollView.contentOffset.y > 20){
        self.backButton.hidden = YES;
        [self.navigationController setNavigationBarHidden:NO animated:YES];
    }
    
    
    lastOffset = scrollView.contentOffset.y;
}

#pragma mark - Table view datasource
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 7;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    WOFRestaurantTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"restaurantDetail"];
    switch (indexPath.row) {
        case 0:
            cell.informationTitle.text = @"Ver menú";
            cell.informationText.text = @"Descubre el menú de este local";
            [cell.favoriteButton setImage:[UIImage imageNamed:@"menu_icon"] forState:UIControlStateNormal];
            [cell.favoriteButton setUserInteractionEnabled:NO];
            [cell.separatorLine setHidden:NO];
            break;
        case 1:
            cell.informationTitle.text = @"Agregar a mis Wenos";
            cell.informationText.text = @"Guarda tus favoritos.";
            [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
            isFavorite ? [cell.favoriteButton setImage:[UIImage imageNamed:@"favorite_fill"] forState:UIControlStateNormal] : [cell.favoriteButton setImage:[UIImage imageNamed:@"favorite_empty"] forState:UIControlStateNormal];
            break;
        case 2:
            cell.informationTitle.text = @"Dirección";
            cell.informationText.text = [NSString stringWithFormat:@"%@",contactInfo];
            [cell.favoriteButton setImage:[UIImage imageNamed:@"contact_icon"] forState:UIControlStateNormal];
            [cell.favoriteButton.imageView setContentMode:UIViewContentModeScaleAspectFit];
            break;
        case 3:
            cell.informationTitle.text = @"Teléfono";
            [cell.favoriteButton setImage:[UIImage imageNamed:@"phone_icon"] forState:UIControlStateNormal];
            cell.informationText.text = [NSString stringWithFormat:@"%@", phoneNumber];
            break;
        case 4:
            cell.informationTitle.text = @"Nuestra historia";
            [cell.favoriteButton setImage:[UIImage imageNamed:@"history_icon"] forState:UIControlStateNormal];
            [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
            cell.informationText.text = detailRestaurant.objectDescription;
            break;
        case 5:
            cell.informationTitle.text = @"Más información";
            [cell.favoriteButton setImage:[UIImage imageNamed:@"more_info_icon"] forState:UIControlStateNormal];
            cell.informationText.text = moreInfo;
            break;
        case 6:
            cell.informationTitle.text = @"WiFi";
            [cell.favoriteButton setImage:[UIImage imageNamed:@"wifi_icon"] forState:UIControlStateNormal];
            [cell.favoriteButton.imageView setContentMode:UIViewContentModeScaleAspectFit];
            cell.informationText.text = [NSString stringWithFormat:@"Clave WiFi: %@ (Tap para copiar clave)", wiFiPassword];
            [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
            break;
        default:
            break;
    }
    return cell;
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    switch (indexPath.row) {
        case 0:
            [self performSegueWithIdentifier:@"showMenu" sender:nil];
            break;
        case 2:{
            CLGeocoder *geocoder = [[CLGeocoder alloc] init];
            NSString *addressString = [NSString stringWithFormat:@"%@, Tijuana, Mexico", detailRestaurant.address];
            [geocoder geocodeAddressString:addressString
                         completionHandler:^(NSArray *placemarks, NSError *error) {
                             CLPlacemark *geocodedPlacemark = [placemarks objectAtIndex:0];
                             MKPlacemark *placemark = [[MKPlacemark alloc]
                                                       initWithCoordinate:geocodedPlacemark.location.coordinate
                                                       addressDictionary:geocodedPlacemark.addressDictionary];
                             MKMapItem *mapItem = [[MKMapItem alloc] initWithPlacemark:placemark];
                             [mapItem setName:detailRestaurant.name];
                             [mapItem openInMapsWithLaunchOptions:nil];
                             
                         }];
            break;
        }
        case 3:{
            NSString *restaurantPhone = [@"tel://" stringByAppendingString:phoneNumber];
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:restaurantPhone]];
            break;
        }
        case 6:
            [UIPasteboard generalPasteboard].string = wiFiPassword;
            [WOFViewHelper presentDefaultAlertViewWithTitle:@"Se copió la contraseña" withMessage:@"Se copió la contraseña del WiFi en tu portapapeles." inViewController:self];
            break;
        default:
            break;
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row <= 1 || indexPath.row == 3) {
        return 60;
    }
    NSString *cellText = [self stringforRow:indexPath.row];
    CGFloat height = [cellText boundingRectWithSize:CGSizeMake(tableView.frame.size.width - 25, 150) options:(NSStringDrawingUsesLineFragmentOrigin|NSStringDrawingUsesFontLeading) attributes:nil context:nil].size.height;
    
    return (height < 50) ? 150 : height + 150;
}

- (NSString *) stringforRow: (NSInteger) row{
    switch (row) {
        case 1:
            return contactInfo;
            break;
        case 2:
            return detailRestaurant.objectDescription;
            break;
        case 3:
            return moreInfo;
        default:
            return nil;
            break;
    }
}

-(BOOL)pointInside:(CGPoint)point withEvent:(UIEvent *)event {
    
    /// the height of the table itself.
    float height = self.infoTableView.frame.size.height + self.infoTableView.contentOffset.y;
    /// the bounds of the table.
    /// it's strange that the origin of the table view is actually the top-left of the table.
    CGRect tableBounds = CGRectMake(0, 0, self.infoTableView.bounds.size.width, height);
    return CGRectContainsPoint(tableBounds, point);
    
}

#pragma mark - Actions

- (IBAction)dismissView:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)markFavorite:(id)sender {
    UIButton *favoriteButton = (UIButton *) sender;
    if (isRegisteredUser) {
        if (!isFavorite) {
            [progressHUD showInView:self.view];
            [objectDataHelper createFavoriteWithRestaurant:detailRestaurant];
            [favoriteButton setImage:[UIImage imageNamed:@"favorite_fill"] forState:UIControlStateNormal];
            isFavorite = YES;
        }else{
            [objectDataHelper removeFavoriteRestaurant:detailRestaurant];
            [favoriteButton setImage:[UIImage imageNamed:@"favorite_empty"] forState:UIControlStateNormal];
            isFavorite = NO;
        }
    }else{
        [WOFViewHelper presentDefaultAlertViewWithTitle:@"No estás registrado" withMessage:@"Sólo los usuarios registrados pueden guardar favoritos." inViewController:self];
    }
    
}

#pragma mark - Data helper delegate

- (void)finishedSavingFavorite{
    [progressHUD dismiss];
    [self.delegate favoriteChanged];
    [WOFViewHelper presentDefaultAlertViewWithTitle:@"Listo" withMessage:@"Se ha guardado tu favorito." inViewController:self];
}

- (void)finishedDeletingFavorite{
    [self.delegate favoriteChanged];
}

#pragma mark - Navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"showMenu"]) {
        /*WOFRestaurantMenuViewController *nextVC = [segue destinationViewController];
         nextVC.restaurant = detailRestaurant;*/
        WOFFoodCollectionViewController *nextVC = [segue destinationViewController];
        nextVC.restaurantID = detailRestaurant.objectID;
        nextVC.restaurantName = detailRestaurant.name;
    }
}

@end
