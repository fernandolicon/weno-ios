//
//  FirstViewController.m
//  WenoApp
//
//  Created by Luis Fernando Mata on 11/6/15.
//  Copyright (c) 2015 Weno. All rights reserved.
//

#import "WOFLogInViewController.h"
#import <JGProgressHUD/JGProgressHUD.h>
#import <GoogleOpenSource/GoogleOpenSource.h>
#import <GooglePlus/GooglePlus.h>
#import "WOFUser.h"
#import "WOFViewHelper.h"
#import "Settings.h"
#import "Haneke.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>

@interface WOFLogInViewController () <GPPSignInDelegate>{
    GPPSignIn *signIn;
    JGProgressHUD *progressHUD;
    __weak IBOutlet UIImageView *logoImage;
}


@end

@implementation WOFLogInViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    self.title = @"LogIn";
    
    progressHUD = [JGProgressHUD progressHUDWithStyle:JGProgressHUDStyleDark];
    progressHUD.textLabel.text = @"Registrando";
    
    //Google LogIn
    signIn = [GPPSignIn sharedInstance];
    signIn.shouldFetchGooglePlusUser = YES;
    signIn.shouldFetchGoogleUserEmail = YES;
    
    signIn.clientID = kClientId;
    
    signIn.scopes = @[ kGTLAuthScopePlusLogin ];
    signIn.delegate = self;
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - LogIn Method Delegates

- (void) setUserDefaultsforLogInMethod: (NSString *) logInMethod{
    [[NSUserDefaults standardUserDefaults] setObject:logInMethod forKey:@"logInMethod"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    [progressHUD dismiss];
    [self performSegueWithIdentifier:@"userLoggedIn" sender:nil];
}

- (void) errorWithAuth{
    [WOFViewHelper presentDefaultAlertViewWithTitle:@"Error" withMessage:@"Hubo algún error con la autenticación, intenta nuevamente." inViewController:self];
}

- (IBAction)logInFacebook:(id)sender {
    FBSDKLoginManager *login = [[FBSDKLoginManager alloc] init];
    [login logInWithReadPermissions:@[@"email"] handler:^(FBSDKLoginManagerLoginResult *result, NSError *error) {
        if (error) {
            [self errorWithAuth];
        } else if (result.isCancelled) {
            [self errorWithAuth];
        } else {
            if ([result.grantedPermissions containsObject:@"email"]) {
                [self getFacebookData];
            }
        }
    }];
}

- (void)finishedWithAuth: (GTMOAuth2Authentication *)auth
                   error: (NSError *) error {
    if (!error) {
        [self getGoogleDatawithEmail: auth.userEmail];
    }else{
        [self errorWithAuth];
    }
}

- (IBAction)logInGoogle:(id)sender {
    [signIn authenticate];
}

- (IBAction)continueWithoutRegister:(id)sender {
    [self setUserDefaultsforLogInMethod:@"NoMethod"];
}

#pragma mark - User Data

- (void) getFacebookData{
    [progressHUD showInView:self.view];
    if ([FBSDKAccessToken currentAccessToken]) {
        [[[FBSDKGraphRequest alloc] initWithGraphPath:@"me" parameters:nil]
         startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, NSDictionary *result, NSError *error) {
             if (!error) {
                 WOFUser *facebookUser = [[WOFUser alloc] initWithFacebookResponse:result];
                 [self registerUserInfo:facebookUser];
                 [self setUserDefaultsforLogInMethod:@"Facebook"];
             }else{
                 [self errorWithAuth];
             }
         }];
    }
}

- (void) getGoogleDatawithEmail: (NSString *) email{
    [progressHUD showInView:self.view];
    
    GTLServicePlus* plusService = [[GTLServicePlus alloc] init];
    plusService.retryEnabled = YES;
    
    [plusService setAuthorizer:[GPPSignIn sharedInstance].authentication];
    
    GTLQueryPlus *query = [GTLQueryPlus queryForPeopleGetWithUserId:@"me"];
    
    [plusService executeQuery:query
            completionHandler:^(GTLServiceTicket *ticket,
                                GTLPlusPerson *person,
                                NSError *error) {
                if (error) {
                    GTMLoggerError(@"Error: %@", error);
                } else {
                    WOFUser *googleUser = [[WOFUser alloc] initWithGooglePerson:person withEmail:email];
                    [self registerUserInfo:googleUser];
                    [self setUserDefaultsforLogInMethod:@"Google"];
                }
            }];
}

- (void) registerUserInfo: (WOFUser *) loggedUser{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *appFile = [documentsDirectory stringByAppendingPathComponent:@"user_info.txt"];
    [NSKeyedArchiver archiveRootObject:loggedUser toFile:appFile];
}

@end
