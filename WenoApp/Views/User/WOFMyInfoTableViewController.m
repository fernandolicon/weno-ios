//
//  WOFMyInfoTableViewController.m
//  WenoApp
//
//  Created by Luis Fernando Mata on 3/7/15.
//  Copyright (c) 2015 Weno. All rights reserved.
//

#import "WOFMyInfoTableViewController.h"
#import "WOFFavoritesCollectionViewController.h"
#import "WOFProfileTableViewCell.h"

@interface WOFMyInfoTableViewController (){
    BOOL selectedRestaurants;
    BOOL isRegisteredUser;
}

@end

@implementation WOFMyInfoTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
    
    //If user is not registered he can't have favorites
    NSString *logInMethod = [[NSUserDefaults standardUserDefaults] objectForKey:@"logInMethod"];
    isRegisteredUser = [logInMethod isEqualToString:@"NoMethod"] ? NO : YES;
    
    !isRegisteredUser ? self.navigationItem.rightBarButtonItem = nil : nil;
    
    //self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return isRegisteredUser ? 2 : 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"otherCell" forIndexPath:indexPath];
    cell.textLabel.font = [UIFont fontWithName:@"Muli" size:17.0f];
    
    if (!isRegisteredUser) {
        cell.textLabel.text = @"Inciar sesión";
    }else{
        cell.textLabel.text = (indexPath.row == 0) ? @"Productos favoritos" : @"Lugares favoritos";
    }
    return cell;
}

#pragma mark - Table view delegate

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 44.0f;
}

- (CGFloat) tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 60.0f;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 30.0f;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{
    return isRegisteredUser ? @"Mis Wenos" : @"";
}

- (NSString *)tableView:(UITableView *)tableView titleForFooterInSection:(NSInteger)section{
    return isRegisteredUser ? @"" :  @"Al iniciar sesión tienes acceso a guardar tus lugares y platillos favoritos para verlos después sin conexión.";
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if (!isRegisteredUser) {
        [self performSegueWithIdentifier:@"userWantstoLogIn" sender:nil];
    }
    
    if (indexPath.row == 1) {
        selectedRestaurants = YES;
    }else{
        selectedRestaurants = NO;
    }
    
    [self performSegueWithIdentifier:@"showFavorites" sender:nil];
}

#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    if ([segue.identifier isEqualToString:@"showFavorites"]) {
        WOFFavoritesCollectionViewController *nextVC = [segue destinationViewController];
        nextVC.selectedRestaurants = selectedRestaurants;
    }
}

@end
