//
//  WOFFavoritesCollectionViewController.h
//  WenoApp
//
//  Created by Luis Fernando Mata on 10/7/15.
//  Copyright (c) 2015 Weno. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WOFFavoritesCollectionViewController : UICollectionViewController

@property BOOL selectedRestaurants;

@end
