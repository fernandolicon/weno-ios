//
//  WOFViewHelper.m
//  WenoApp
//
//  Created by Luis Fernando Mata on 10/10/15.
//  Copyright © 2015 Weno. All rights reserved.
//

#import "WOFViewHelper.h"

@implementation WOFViewHelper

+ (void) presentDefaultAlertViewWithTitle: (NSString *) title withMessage: (NSString *) message inViewController: (UIViewController *) viewController{
    UIAlertController *alertView = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *okButton = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault handler:nil];
    [alertView addAction:okButton];
    [viewController presentViewController:alertView animated:YES completion:nil];
}

+ (NSString *) nameForType: (NSString *) type{
    
    if ([type isEqualToString:@"foodGeneral"]){
        return @"General";
    }
    
    if ([type isEqualToString:@"foodComida"]) {
        return @"Comida";
    }
    
    if ([type isEqualToString:@"foodCena"]) {
        return @"Cena";
    }
    
    if ([type isEqualToString:@"foodPlatoFuerte"]) {
        return @"Plato Fuerte";
    }
    
    if ([type isEqualToString:@"foodPostre"]) {
        return @"Postre";
    }
    
    if ([type isEqualToString:@"foodBebida"]) {
        return @"Bebida";
    }
    
    if ([type isEqualToString:@"foodDesayuno"]) {
        return @"Desayuno";
    }
    
    if ([type isEqualToString:@"foodEntrada"]) {
        return @"Entrada";
    }
    
    return @"";
}

@end
