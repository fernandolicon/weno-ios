//
//  WOFRequestHelper.h
//  WenoApp
//
//  Created by Luis Fernando Mata on 23/6/15.
//  Copyright (c) 2015 Weno. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AFHTTPRequestOperationManager.h"
#import "Settings.h"

@protocol WOFRequestHelperDelegate <NSObject>
- (void) requestDidFinishRequestingData: (NSDictionary *) responseDictionary performSelector: (NSString *) selector;
- (void) requestErrorInConnection;
@end

@interface WOFRequestHelper : NSObject

@property (nonatomic, weak) id <WOFRequestHelperDelegate> delegate;

- (void) listofAllPlacesPerformSelector: (NSString *) selector;
- (void) listofPlacesBy: (NSString *) userSelection forType: (NSString *) type performSelector: (NSString *) selector;
- (void) restaurantType: (NSString *) userSelection performSelector: (NSString *) selector;
- (void) detailforPlace: (NSString *) placeName performSelector: (NSString *) selector;
- (void) detailforRestaurantWithID: (NSInteger) restID performSelector: (NSString *) selector;
- (void) detailforFood: (NSString *) foodName performSelector: (NSString *) selector;
- (void) menuforRestaurantwithId: (NSInteger) restaurantID performSelector: (NSString *) selector;
- (void) searchString: (NSString *) searchString performSelector: (NSString *) selector;
- (void) searchRestaurantforCode: (NSString *) scannedCode performSelector: (NSString *) selector;

#pragma Block methods
//TODO: Change all the other methods to this type
+ (void)getMenuForRestaurant: (NSInteger) restId withSuccess:(void (^)(NSDictionary *response))success withFailure: (void (^)(NSError *error))failure;

@end
