//
//  WOFObjectCoreDataHelper.h
//  WenoApp
//
//  Created by Luis Fernando Mata on 10/7/15.
//  Copyright (c) 2015 Weno. All rights reserved.
//

#import <Foundation/Foundation.h>
@class WOFRestaurantCoreData;
@class WOFFoodCoreData;
@class WOFRestaurant;
@class WOFFood;

@protocol WOFObjectCoreDataHelperDelegate <NSObject>
@optional
- (void) finishedSavingFavorite;
- (void) finishedDeletingFavorite;
- (void) finishedErasingDB;
@end

@interface WOFObjectCoreDataHelper : NSObject

@property (nonatomic, weak) id <WOFObjectCoreDataHelperDelegate> delegate;

#pragma mark - Restaurant methods
- (void) createFavoriteWithRestaurant: (WOFRestaurant *) favoriteRestaurant;
- (void) removeFavoriteRestaurant: (WOFRestaurant *) removeRestaurant;
- (NSMutableArray *) transformDictionaryofFavorites: (NSArray *) favoriteRestaurants;
- (NSArray *) findAllRestaurants;
- (BOOL) isRestaurantwithIDFavorite: (NSInteger) restID;
- (NSDictionary *) restaurantPicturesWithID: (NSInteger) restID;

#pragma mark - Food methods
- (void) createFavoriteWithFood: (WOFFood *) favoriteFood;
- (void) removeFavoriteFood: (WOFFood *) removeFood;
- (NSMutableArray *) transformDictionaryofFavoriteFoods: (NSArray *) favoriteFoods;
- (NSArray *) findAllFoods;
- (BOOL) isFoodwithIDFavorite: (NSInteger) foodID;
- (NSData *) foodPictureWithID: (NSInteger) foodID;

#pragma mark - Database methods
- (void) eraseAllInfoinDataBase;

@end