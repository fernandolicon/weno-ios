//
//  WOFObjectCoreDataHelper.m
//  WenoApp
//
//  Created by Luis Fernando Mata on 10/7/15.
//  Copyright (c) 2015 Weno. All rights reserved.
//

#import "WOFObjectCoreDataHelper.h"
#import <MagicalRecord/MagicalRecord.h>
#import "WOFFoodCoreData.h"
#import "WOFRestaurantCoreData.h"
#import "WOFRestaurant.h"
#import "WOFFood.h"

@implementation WOFObjectCoreDataHelper

#pragma  mark - Restaurant methods
- (void) createFavoriteWithRestaurant: (WOFRestaurant *) favoriteRestaurant{
    [MagicalRecord saveWithBlock:^(NSManagedObjectContext *localContext) {
        WOFRestaurantCoreData *newFavorite = [WOFRestaurantCoreData MR_createEntityInContext:localContext];
        
        newFavorite.name = favoriteRestaurant.name;
        newFavorite.address = favoriteRestaurant.address;
        newFavorite.amountPreference = @(favoriteRestaurant.amountPreference);
        newFavorite.bannerPhoto = [NSData dataWithContentsOfURL:favoriteRestaurant.bannerPhoto];
        newFavorite.bannerPhotoURL = [NSString stringWithFormat:@"%@",favoriteRestaurant.bannerPhoto];
        newFavorite.codeDress = favoriteRestaurant.codeDress;
        newFavorite.information = favoriteRestaurant.information;
        newFavorite.objectDescription = favoriteRestaurant.objectDescription;
        newFavorite.phone = favoriteRestaurant.phone;
        newFavorite.primaryPhoto = [NSData dataWithContentsOfURL:favoriteRestaurant.primaryPhoto];
        newFavorite.primaryPhotoURL = [NSString stringWithFormat:@"%@", favoriteRestaurant.primaryPhoto];
        newFavorite.restID = @(favoriteRestaurant.objectID);
        newFavorite.restZone = favoriteRestaurant.zone;
        newFavorite.typeMenu = favoriteRestaurant.typeMenu;
        newFavorite.wifiPassword = favoriteRestaurant.wifiPassword;
    }completion:^(BOOL contextDidSave, NSError *error) {
        [self.delegate finishedSavingFavorite];
    }];
}

- (NSMutableArray *) transformDictionaryofFavorites: (NSArray *) favoriteRestaurants{
    NSMutableArray *restaurants = [[NSMutableArray alloc] init];
    
    for (WOFRestaurantCoreData *favorite in favoriteRestaurants) {
        WOFRestaurant *provisionalRestaurant = [[WOFRestaurant alloc] init];
        provisionalRestaurant.name = favorite.name;
        provisionalRestaurant.objectDescription = favorite.objectDescription;
        provisionalRestaurant.objectID = [favorite.restID integerValue];
        provisionalRestaurant.primaryPhotoData = favorite.primaryPhoto;
        provisionalRestaurant.primaryPhoto = [NSURL URLWithString:favorite.primaryPhotoURL];
        provisionalRestaurant.bannerPhotoData = favorite.bannerPhoto;
        provisionalRestaurant.bannerPhoto = [NSURL URLWithString:favorite.bannerPhotoURL];
        provisionalRestaurant.information = favorite.information;
        provisionalRestaurant.isRestaurant = YES;
        provisionalRestaurant.address = favorite.address;
        provisionalRestaurant.phone = favorite.phone;
        provisionalRestaurant.wifiPassword = favorite.wifiPassword;
        provisionalRestaurant.codeDress = favorite.codeDress;
        provisionalRestaurant.zone = favorite.restZone;
        provisionalRestaurant.typeMenu = favorite.typeMenu;
        provisionalRestaurant.amountPreference = [favorite.amountPreference integerValue];
        
        [restaurants addObject:provisionalRestaurant];
    }
    
    return restaurants;
}

- (void) removeFavoriteRestaurant: (WOFRestaurant *) removeRestaurant{
    [MagicalRecord saveWithBlock:^(NSManagedObjectContext *localContext) {
        WOFRestaurantCoreData *restaurant = [WOFRestaurantCoreData MR_findFirstByAttribute:@"restID"
                                                                                         withValue:[NSString stringWithFormat:@"%ld", (long) removeRestaurant.objectID]];
        [restaurant MR_deleteEntityInContext:localContext];
    }completion:^(BOOL contextDidSave, NSError *error) {
        [self.delegate finishedDeletingFavorite];
    }];
}

- (NSArray *) findAllRestaurants{
    return [WOFRestaurantCoreData MR_findAllSortedBy:@"name" ascending:YES];
}

- (BOOL) isRestaurantwithIDFavorite: (NSInteger) restID{
    WOFRestaurantCoreData *restaurant = [WOFRestaurantCoreData MR_findFirstByAttribute:@"restID"
                                           withValue:[NSString stringWithFormat:@"%ld", (long) restID]];
    return (restaurant) ? YES : NO;
}

- (NSDictionary *) restaurantPicturesWithID: (NSInteger) restID{
    WOFRestaurantCoreData *restaurant = [WOFRestaurantCoreData MR_findFirstByAttribute:@"restID" withValue:[NSString stringWithFormat:@"%ld", (long) restID]];
    NSDictionary *imagesDictionary = @{@"banner" : restaurant.bannerPhoto, @"logo" : restaurant.primaryPhoto};
    
    return imagesDictionary;
}

#pragma mark - Food methods
- (void) createFavoriteWithFood: (WOFFood *) favoriteFood{
    [MagicalRecord  saveWithBlock:^(NSManagedObjectContext *localContext) {
        WOFFoodCoreData *newFavorite = [WOFFoodCoreData MR_createEntityInContext:localContext];
        newFavorite.bannerPhoto = [NSData dataWithContentsOfURL:favoriteFood.bannerPhoto];
        newFavorite.bannerPhotoURL = [NSString stringWithFormat:@"%@", favoriteFood.bannerPhoto];
        newFavorite.diet = favoriteFood.diet;
        newFavorite.foodID = @(favoriteFood.objectID);
        newFavorite.information = favoriteFood.information;
        newFavorite.name = favoriteFood.name;
        newFavorite.objectDescription = favoriteFood.objectDescription;
        newFavorite.prefered = @(favoriteFood.prefered);
        newFavorite.price = @(favoriteFood.price);
        newFavorite.primaryPhoto = [NSData dataWithContentsOfURL:favoriteFood.primaryPhoto];
        newFavorite.primaryPhotoURL = [NSString stringWithFormat:@"%@", favoriteFood.primaryPhoto];
        newFavorite.restaurantID = @(favoriteFood.restaurantID);
        newFavorite.timeCook = favoriteFood.timeCook;
        newFavorite.typeCook = favoriteFood.typeCook;
        newFavorite.restaurantName = favoriteFood.restaurantName;
    }completion:^(BOOL contextDidSave, NSError *error) {
        [self.delegate finishedSavingFavorite];
    }];
}

- (void) removeFavoriteFood: (WOFFood *) removeFood{
    [MagicalRecord saveWithBlock:^(NSManagedObjectContext *localContext) {
        WOFFoodCoreData *food = [WOFFoodCoreData MR_findFirstByAttribute:@"foodID" withValue:[NSString stringWithFormat:@"%ld", (long) removeFood.objectID]];
        [food MR_deleteEntityInContext:localContext];
    } completion:^(BOOL contextDidSave, NSError *error) {
        [self.delegate finishedDeletingFavorite];
    }];
}

- (NSMutableArray *) transformDictionaryofFavoriteFoods: (NSArray *) favoriteFoods{
    NSMutableArray *foods = [[NSMutableArray alloc] init];
    
    for (WOFFoodCoreData *favorite in favoriteFoods) {
        WOFFood *provisionalFood = [[WOFFood alloc] init];
        
        provisionalFood.name = favorite.name;
        provisionalFood.objectDescription = favorite.objectDescription;
        provisionalFood.primaryPhotoData = favorite.primaryPhoto;
        provisionalFood.primaryPhoto = [NSURL URLWithString:favorite.primaryPhotoURL];
        provisionalFood.bannerPhotoData = favorite.bannerPhoto;
        provisionalFood.bannerPhoto = [NSURL URLWithString:favorite.bannerPhotoURL];
        provisionalFood.information = favorite.information;
        provisionalFood.objectID = [favorite.foodID integerValue];
        provisionalFood.typeCook = favorite.typeCook;
        provisionalFood.restaurantID = [favorite.restaurantID integerValue];
        provisionalFood.prefered = [favorite.prefered boolValue];
        provisionalFood.price = [favorite.price floatValue];
        provisionalFood.timeCook = favorite.timeCook;
        provisionalFood.diet = favorite.diet;
        provisionalFood.restaurantName = favorite.restaurantName;
        
        [foods addObject:provisionalFood];
    }
    
    return foods;
}

- (NSArray *) findAllFoods{
    return [WOFFoodCoreData MR_findAllSortedBy:@"name"ascending:YES];
}

- (BOOL) isFoodwithIDFavorite: (NSInteger) foodID{
    WOFFoodCoreData *food = [WOFFoodCoreData MR_findFirstByAttribute:@"foodID" withValue:[NSString stringWithFormat:@"%ld", (long) foodID]];
    return (food) ? YES : NO;
}

- (NSData *) foodPictureWithID: (NSInteger) foodID{
    WOFFoodCoreData *food = [WOFFoodCoreData MR_findFirstByAttribute:@"foodID" withValue:[NSString stringWithFormat:@"%ld", (long) foodID]];
    
    return food.primaryPhoto;
}

#pragma mark - Database methods
- (void) eraseAllInfoinDataBase{
    [MagicalRecord saveWithBlock:^(NSManagedObjectContext *localContext) {
        [WOFFoodCoreData MR_truncateAllInContext:localContext];
        [WOFRestaurantCoreData MR_truncateAllInContext:localContext];
    } completion:^(BOOL contextDidSave, NSError *error) {
        [self.delegate finishedErasingDB];
    }];
}

@end
