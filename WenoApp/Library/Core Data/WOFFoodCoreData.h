//
//  WOFFoodCoreData.h
//  WenoApp
//
//  Created by Luis Fernando Mata on 31/7/15.
//  Copyright (c) 2015 Weno. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface WOFFoodCoreData : NSManagedObject

@property (nonatomic, retain) NSData * bannerPhoto;
@property (nonatomic, retain) NSString * bannerPhotoURL;
@property (nonatomic, retain) NSString * diet;
@property (nonatomic, retain) NSNumber * foodID;
@property (nonatomic, retain) NSString * information;
@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSString * objectDescription;
@property (nonatomic, retain) NSNumber * prefered;
@property (nonatomic, retain) NSNumber * price;
@property (nonatomic, retain) NSData * primaryPhoto;
@property (nonatomic, retain) NSString * primaryPhotoURL;
@property (nonatomic, retain) NSNumber * restaurantID;
@property (nonatomic, retain) NSString * timeCook;
@property (nonatomic, retain) NSString * typeCook;
@property (nonatomic, retain) NSString * restaurantName;

@end
