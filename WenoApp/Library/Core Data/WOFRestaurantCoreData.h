//
//  WOFRestaurantCoreData.h
//  WenoApp
//
//  Created by Luis Fernando Mata on 31/7/15.
//  Copyright (c) 2015 Weno. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface WOFRestaurantCoreData : NSManagedObject

@property (nonatomic, retain) NSString * address;
@property (nonatomic, retain) NSNumber * amountPreference;
@property (nonatomic, retain) NSData * bannerPhoto;
@property (nonatomic, retain) NSString * bannerPhotoURL;
@property (nonatomic, retain) NSString * codeDress;
@property (nonatomic, retain) NSString * information;
@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSString * objectDescription;
@property (nonatomic, retain) NSString * phone;
@property (nonatomic, retain) NSData * primaryPhoto;
@property (nonatomic, retain) NSString * primaryPhotoURL;
@property (nonatomic, retain) NSNumber * restID;
@property (nonatomic, retain) NSString * restZone;
@property (nonatomic, retain) NSString * typeMenu;
@property (nonatomic, retain) NSString * wifiPassword;

@end
