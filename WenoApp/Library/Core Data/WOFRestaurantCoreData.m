//
//  WOFRestaurantCoreData.m
//  WenoApp
//
//  Created by Luis Fernando Mata on 31/7/15.
//  Copyright (c) 2015 Weno. All rights reserved.
//

#import "WOFRestaurantCoreData.h"


@implementation WOFRestaurantCoreData

@dynamic address;
@dynamic amountPreference;
@dynamic bannerPhoto;
@dynamic bannerPhotoURL;
@dynamic codeDress;
@dynamic information;
@dynamic name;
@dynamic objectDescription;
@dynamic phone;
@dynamic primaryPhoto;
@dynamic primaryPhotoURL;
@dynamic restID;
@dynamic restZone;
@dynamic typeMenu;
@dynamic wifiPassword;

@end
