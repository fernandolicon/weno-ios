//
//  WOFFoodCoreData.m
//  WenoApp
//
//  Created by Luis Fernando Mata on 31/7/15.
//  Copyright (c) 2015 Weno. All rights reserved.
//

#import "WOFFoodCoreData.h"


@implementation WOFFoodCoreData

@dynamic bannerPhoto;
@dynamic bannerPhotoURL;
@dynamic diet;
@dynamic foodID;
@dynamic information;
@dynamic name;
@dynamic objectDescription;
@dynamic prefered;
@dynamic price;
@dynamic primaryPhoto;
@dynamic primaryPhotoURL;
@dynamic restaurantID;
@dynamic timeCook;
@dynamic typeCook;
@dynamic restaurantName;

@end
