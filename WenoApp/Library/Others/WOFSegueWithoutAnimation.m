//
//  WOFSegueWithoutAnimation.m
//  WenoApp
//
//  Created by Luis Fernando Mata on 7/21/15.
//  Copyright (c) 2015 Weno. All rights reserved.
//

#import "WOFSegueWithoutAnimation.h"

@implementation WOFSegueWithoutAnimation

-(void) perform{
    UIViewController *vc = self.sourceViewController;
    [vc.navigationController pushViewController:self.destinationViewController animated:NO];
}

@end
