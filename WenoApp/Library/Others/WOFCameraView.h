//
//  WOFCameraView.h
//  WenoApp
//
//  Created by Luis Fernando Mata on 13/6/15.
//  Copyright (c) 2015 Weno. All rights reserved.
//

#import <UIKit/UIKit.h>

@class AVCaptureSession;

@interface WOFCameraView : UIView

@property (nonatomic) AVCaptureSession *session;

@end
