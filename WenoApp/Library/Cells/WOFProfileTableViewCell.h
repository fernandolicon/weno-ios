//
//  WOFProfileTableViewCell.h
//  WenoApp
//
//  Created by Luis Fernando Mata on 2/7/15.
//  Copyright (c) 2015 Weno. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <WHRoundedImageView/WHRoundedImageView-Swift.h>

@interface WOFProfileTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet WHRoundedImageView *userProfilePicture;
@property (weak, nonatomic) IBOutlet UILabel *userName;

@end
