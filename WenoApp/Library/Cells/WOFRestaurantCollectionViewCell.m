//
//  WOFRestaurantCollectionViewCell.m
//  WenoApp
//
//  Created by Luis Fernando Mata on 29/6/15.
//  Copyright (c) 2015 Weno. All rights reserved.
//

#import "WOFRestaurantCollectionViewCell.h"
#import "WOFObject.h"
#import "WOFRestaurant.h"
#import "WOFFood.h"
#import <Haneke/Haneke.h>

@implementation WOFRestaurantCollectionViewCell

- (void) createCellWithObject: (WOFObject *) cellObject{
    self.objectName.text = cellObject.name;
    UIImage *whiteImage = [UIImage imageNamed:@"white_placeholder"];
    
    if (cellObject.isRestaurant) {
        [self.bannerImage hnk_setImageFromURL:cellObject.bannerPhoto placeholder:whiteImage];
        [self.logoImage setHidden:NO];
        [self.logoImage hnk_setImageFromURL:cellObject.primaryPhoto placeholder:whiteImage];
        [self roundImage];
        WOFRestaurant *provisionalRest = (WOFRestaurant *) cellObject;
        self.descriptionOne.text = provisionalRest.typeMenu;
        self.descriptionTwo.text = provisionalRest.zone;
    }else{
        [self.bannerImage hnk_setImageFromURL:cellObject.primaryPhoto placeholder:whiteImage];
        [self.logoImage setHidden:YES];
        WOFFood *provisionalFood = (WOFFood *) cellObject;
        self.descriptionOne.text = [NSString stringWithFormat:@"$%.2f", provisionalFood.price];
        self.descriptionTwo.text = provisionalFood.restaurantName;
    }
    
    [self.contentView.layer setBorderColor:[UIColor blackColor].CGColor];
    [self.contentView.layer setBorderWidth:0.5f];
}

- (void) roundImage{
    //Something is wrong with auto layout, so we will calculate sizes and frames in order to make the circle look good
    CGFloat width = self.frame.size.width;
    CGFloat height = (3.0f / 4.0f) * width;
    CGRect bannerRect = CGRectMake(0, 0, self.frame.size.width, height);
    [self.bannerImage setFrame:bannerRect];
    width = self.bannerImage.frame.size.width / 2.0f;
    CGRect logoImageRect = CGRectMake(self.logoImage.frame.origin.x, self.logoImage.frame.origin.y, width, width);
    [self.logoImage setFrame:logoImageRect];
    self.logoImage.radiusRatio = 0.5f;
}

- (void)prepareForReuse{
    self.logoImage.radiusRatio = 0.0f;
}

@end
