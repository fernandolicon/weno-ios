//
//  WOFRestaurant.m
//  WenoApp
//
//  Created by Luis Fernando Mata on 23/6/15.
//  Copyright (c) 2015 Weno. All rights reserved.
//

#import "WOFRestaurant.h"

@implementation WOFRestaurant

- (instancetype) initWithDictionary: (NSDictionary *) restaurantDictionary{
    if (self = [super init]) {
        self.name = [restaurantDictionary objectForKey:@"name"];
        self.objectDescription = [restaurantDictionary objectForKey:@"biography"];
        self.address = [restaurantDictionary objectForKey:@"address"];
        self.address = [self.address stringByReplacingOccurrencesOfString:@"\\n" withString:@"\n"];
        self.phone = [restaurantDictionary objectForKey:@"phone"];
        self.information = [restaurantDictionary objectForKey:@"information"];
        NSString *auxString = [restaurantDictionary objectForKey:@"primary_photo"];
        self.primaryPhoto = [NSURL URLWithString:auxString];
        auxString = [restaurantDictionary objectForKey:@"banner_photo"];
        self.bannerPhoto = [NSURL URLWithString:auxString];
        self.wifiPassword = [restaurantDictionary objectForKey:@"password_wifi"];
        self.codeDress = [restaurantDictionary objectForKey:@"code_dress"];
        self.zone = [restaurantDictionary objectForKey:@"zone"];
        self.typeMenu = [restaurantDictionary objectForKey:@"typemenu"];
        auxString = [restaurantDictionary objectForKey:@"amount_preference"];
        self.amountPreference = [auxString integerValue];
        auxString = [restaurantDictionary objectForKey:@"id"];
        self.objectID = [auxString integerValue];
        self.isRestaurant = YES;
    }
    
    return self;
}

- (instancetype) initWithAlternativeDictionary: (NSDictionary *) restaurantDictionary{
    if (self = [super init]) {
        self.name = [restaurantDictionary objectForKey:@"restaurant"];
        self.objectDescription = [restaurantDictionary objectForKey:@"rest_biography"];
        self.address = [restaurantDictionary objectForKey:@"rest_address"];
        self.address = [self.address stringByReplacingOccurrencesOfString:@"\\n" withString:@"\n"];
        self.phone = [restaurantDictionary objectForKey:@"rest_phone"];
        self.information = [restaurantDictionary objectForKey:@"rest_information"];
        NSString *auxString = [restaurantDictionary objectForKey:@"rest_photo"];
        self.primaryPhoto = [NSURL URLWithString:auxString];
        auxString = [restaurantDictionary objectForKey:@"rest_banner"];
        self.bannerPhoto = [NSURL URLWithString:auxString];
        self.wifiPassword = [restaurantDictionary objectForKey:@"password_wifi"];
        self.codeDress = [restaurantDictionary objectForKey:@"rest_dress"];
        self.zone = [restaurantDictionary objectForKey:@"rest_zone"];
        self.typeMenu = [restaurantDictionary objectForKey:@"rest_typemenu"];
        auxString = [restaurantDictionary objectForKey:@"rest_amount"];
        self.amountPreference = [auxString integerValue];
        auxString = [restaurantDictionary objectForKey:@"rest_id"];
        self.objectID = [auxString integerValue];
        self.isRestaurant = YES;
    }
    
    return self;
}

@end
