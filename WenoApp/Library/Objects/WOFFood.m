//
//  WOFFood.m
//  WenoApp
//
//  Created by Luis Fernando Mata on 23/6/15.
//  Copyright (c) 2015 Weno. All rights reserved.
//

#import "WOFFood.h"

@implementation WOFFood

- (instancetype) initWithDictionary: (NSDictionary *) foodDictionary{
    if (self = [super init]) {
        self.name = [foodDictionary objectForKey:@"name"];
        self.objectDescription = [foodDictionary objectForKey:@"description"];
        self.typeCook = [foodDictionary objectForKey:@"typecook"];
        NSString *auxString = [foodDictionary objectForKey:@"primary_photo"];
        self.primaryPhoto = [NSURL URLWithString:auxString];
        auxString = [foodDictionary objectForKey:@"banner_photo"];
        self.bannerPhoto = [NSURL URLWithString:auxString];
        auxString = [foodDictionary objectForKey:@"restaurantId"];
        self.restaurantID = [auxString integerValue];
        self.restaurantName = [foodDictionary objectForKey:@"restaurant"];
        auxString = [foodDictionary objectForKey:@"preference"];
        NSInteger preferenceNumber = [auxString integerValue];
        self.prefered = (preferenceNumber == 1) ? YES : NO;
        auxString = [foodDictionary objectForKey:@"price"];
        self.price = [auxString doubleValue];
        self.timeCook = [foodDictionary objectForKey:@"timecook"];
        self.diet = [foodDictionary objectForKey:@"diet"];
        self.information = [foodDictionary objectForKey:@"information"];
        self.isRestaurant = NO;
        self.objectID = [[foodDictionary objectForKey:@"id"] integerValue];
    }
    
    return self;
}

- (instancetype) initForMenuWithDictionary: (NSDictionary *) foodDictionary forRestaurantName: (NSString *) restaurantName{
    if (self = [super init]) {
        self.name = [foodDictionary objectForKey:@"food"];
        self.objectDescription = [foodDictionary objectForKey:@"fd_description"];
        NSString *auxString = [foodDictionary objectForKey:@"fd_photo"];
        self.primaryPhoto = [NSURL URLWithString:auxString];
        auxString = [foodDictionary objectForKey:@"fd_banner"];
        self.bannerPhoto = [NSURL URLWithString:auxString];
        auxString = [foodDictionary objectForKey:@"fd"];
        NSInteger preferenceNumber = [auxString integerValue];
        self.prefered = (preferenceNumber == 1) ? YES : NO;
        auxString = [foodDictionary objectForKey:@"fd_price"];
        self.price = [auxString doubleValue];
        self.timeCook = [foodDictionary objectForKey:@"fd_cook"];
        self.diet = [foodDictionary objectForKey:@"fd_diet"];
        self.information = [foodDictionary objectForKey:@"fd_information"];
        self.isRestaurant = NO;
        self.restaurantID = [[foodDictionary objectForKey:@"fd_restaurantid"] integerValue];
        self.restaurantName = restaurantName;

    }
    
    return self;
}

@end
