//
//  main.m
//  WenoApp
//
//  Created by Luis Fernando Mata on 11/6/15.
//  Copyright (c) 2015 Weno. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WOFAppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([WOFAppDelegate class]));
    }
}
